<!DOCTYPE html>
<?php
  include('../shared/conn.php');
  $user = $_SESSION['Username'];
  $sql_n = "SELECT * FROM news";

  $m_news = "SELECT COUNT(ID) AS mynews FROM newsC WHERE User='$user'";
  $res_mn = $conn->query($m_news);
  $count_mn = mysqli_fetch_array($res_mn);
  $my_news = $count_mn['mynews'];

  $ncount_q = "SELECT COUNT(ID) AS tot FROM news";
  $res_q = $conn->query($ncount_q);
  $count = mysqli_fetch_array($res_q);
  $countN = $count['tot'];
  $res_n = $conn->query($sql_n);


  $m_dis = "SELECT COUNT(ID) AS mydis FROM scontiC WHERE User='$user'";
  $res_dis = $conn->query($m_dis);
  $count_dis = mysqli_fetch_array($res_dis);
  $my_dis = $count_dis['mydis'];

  $dcount_d = "SELECT COUNT(ID) AS totD FROM sconti";
  $res_d = $conn->query($dcount_d);
  $count_d = mysqli_fetch_array($res_d);
  $countD = $count_d['totD'];



  $nToShow = $countN - $my_news;
  $dToShow = $countD - $my_dis;

  $ordini_q = "SELECT COUNT(ID) as totOrdini FROM ordiniC WHERE User = '$user'";
  $ordini_res = $conn->query($ordini_q);
  $countOrdini = mysqli_fetch_array($ordini_res);
  $countO = $countOrdini['totOrdini'];

  $ordine_q = "SELECT ID,NumeroOrdine, ProdottiOrdinati, Totale, Indirizzo FROM ordiniC WHERE User = '$user'";
  $ordine_res = $conn->query($ordine_q);
 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>personalPage</title>
    <link rel="stylesheet" href="personalPage.css">
    <link rel="stylesheet" href="tableOrdini.css">
    <script src="jquery-3.2.1.min.js"></script>
    <script  src="../home/newsRotation.js"></script>
    <script src="chooseFood.js"></script>
    </head>
  <body>
    <header>
      <?php include("../shared/header_client.php") ?>
      <div id="header_personal">
          <div>
            <label for="ordConfermati"><input id="ordConfermati" type="button" name="ordConfermati" value="Ordini Ricevuti" onclick="showReceived()"><?php echo $countO ?></label>
            <label for="newsB"><input id="newsB" type="button" name="newsB" value="News" onclick="showNews()"><?php echo $nToShow ?></label>
            <label for="disB"><input id="disB" type="button" name="visSconti" value="Visualizza Sconti" onclick="showDiscounts()"><?php echo $dToShow ?></label>
         </div>
         <div>
           <h1>Benvenuto nella tua pagina, <?php echo $_SESSION['Username'] ?> !</h1>
         </div>
     </div>
    </header>
    <div id="inf">
      <?php include("tableOrdini.php") ?>
      <?php include("../home/news.php") ?>
      <?php include("discounts.php") ?>
    </div>
    <script>
      var_n = <?php echo json_encode($v2); ?>;
    </script>
    <section id="choice1">
        <div id="choice">
          <h1>Trova il tuo cibo preferito!</h1>
          <form action="personalPage.php" method="get">
          <select name="c_select" class="form-control">
            <option value="Pizza">Pizza</option>
            <option value="Dolci">Dolci</option>
            <option value="Bevande">Bevande</option>
            <option value="Burrito">Burrito</option>
            <option value="Hot Dog">Hot Dogs</option>
            <option value="Insalate">Insalate</option>
            <option value="Fritti">Fritti</option>
            <option value="Sandwich">Sandwich</option>
            <option value="Hamburger">Hamburger</option>
          </select>
          <input id="search" type="submit" class="btn_primary" value="Vai!">
          </form>
        </div>
        <?php
            $index = 3;
            $cat = "";
            if(isset($_GET['c_select'])){
                $cat = $_GET['c_select'];
                $sql_i = "SELECT ID AS c_i FROM Categorie WHERE Nome='$cat'";
                $r_i  = $conn->query($sql_i);
                $r_i2= mysqli_fetch_array($r_i);
                $index = $r_i2['c_i'];
                $sql_cat = "SELECT * FROM Prodotti WHERE Categoria='$index'";
                $result_a = $conn->query($sql_cat);
                $v1 = array();
                $v2 = array();
                if($result_a->num_rows > 0){
                  while($row = $result_a->fetch_assoc()){
                    foreach ($row as $v => $x){
                      array_push($v1, $x);
                    }
                    array_push($v2,$v1);
                    $v1 = array();
                    }
                }
            }
         ?>
         <script>
             var ar_f = <?php echo json_encode($v2);?>;
         </script>


      <div id="choosePane">
          <div id="prev">
          <button type="button" class="btn_primary">P</button>
        </div>
            <div>
          <img id="p" src="pizza1.png" alt="Not Available">
          <img id="c" src="pizza2.png" alt="Not Available">
          <img id="n" src="pizza3.png" alt="Not Available">
          </div>
          <div id="next">
            <button type="button" class="btn_primary">N</button>
        </div>
        </div>
      </section>
      <?php
        $f_d = "SELECT Sconto AS d FROM sconti WHERE Categoria = '$index'";
        $res_d = $conn->query($f_d);
        $res_d1 = mysqli_fetch_array($res_d);
        $dis = $res_d1['d'];
       ?>
      <section>
      <div id="mainDiv">
        <div id="showIm" >
            <img src="" alt="">
        </div>
        <div>
          <span class="desc">Descrizione</span>
          <p id="description"></p>
          <span class="desc">Ingredienti</span>
          <p id="ing"></p>
          <span class="desc">Prezzo base</span>
          <p id="price"></p>
          <span>
            <?php
               if($dis != null){
                 echo "La seguente categoria di cibo ha uno sconto del " . $dis ."%. sul prezzo base!";
               }
             ?>
          </span>
        </div>
        <div>
          <input id="Add" type="button" class="btn_primary" value="Add To Cart">
        </div>
        <?php
            if(isset($_GET['prodotto']) && isset($_GET['categoria'])){
              $prodotto = $_GET['prodotto'];
              $categoria = $_GET['categoria'];
              $insert = "INSERT INTO Ordini (User, Prodotto, Categoria) VALUES ('$user', '$prodotto', '$categoria')";
              $conn->query($insert);
            }
       ?>
       <?php
         $a_n = "SELECT ID FROM news";
         $res_news = $conn->query($a_n);
         if(isset($_GET['DONE_N'])){
           if($_GET['DONE_N'] == "OK"){
             if($res_news->num_rows > 0){
               while($row = $res_news->fetch_assoc()){
                 foreach ($row as $v => $x){
                   $f_n = "SELECT ID FROM newsC WHERE User = '$user' AND News='$x'";
                   $find_news = $conn->query($f_n);
                   if($find_news->num_rows == 0){
                     $i_n = "INSERT INTO newsC (News, User) VALUES ('$x','$user')";
                     $conn->query($i_n);
                   }
                 }
               }
             }
           }
         }


         $a_d = "SELECT ID FROM sconti";
         $res_sconti = $conn->query($a_d);
         if(isset($_GET['DONE_D'])){
           if($_GET['DONE_D'] == "OK"){
             if($res_sconti->num_rows > 0){
               while($rowd = $res_sconti->fetch_assoc()){
                 foreach ($rowd as $vd => $xd){
                   $f_d = "SELECT ID FROM scontiC WHERE User = '$user' AND Sconto='$xd'";
                   $find_sconti = $conn->query($f_d);
                   if($find_sconti->num_rows == 0){
                     $i_sconti = "INSERT INTO scontiC (Sconto, User) VALUES ('$xd','$user')";
                     $conn->query($i_sconti);
                   }
                 }
               }
             }
           }
         }
        ?>
      </div>
      </section>
  </body>
</html>
