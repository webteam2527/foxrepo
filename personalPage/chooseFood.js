var count = 0;
var c_im = 5;
var length;

$(document).ready(function(){
  length = ar_f.length;
  $("#login").hide();
  $("#admin").hide();
  $("#signup").hide();
  $("#back").hide();
  $("#logout").show();
  $("#cart").show();
  $("#home").show();

  $("#news").hide();
  $("#discounts").hide();
  $("#ordiniC").hide();
  $("#next .btn_primary").click(getNext);
  $("#prev .btn_primary").click(getPrev);
  $("#Add").click(addFood);
  $("#Cart").click(openCart);
  show();
});

function showNews(){
  $("#news").fadeToggle();
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "personalPage.php?DONE_N=OK", true);
  xmlhttp.send();
}

function showDiscounts(){
  $("#discounts").fadeToggle();
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "personalPage.php?DONE_D=OK", true);
  xmlhttp.send();
}

function ordineRicevuto(id){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "personalPage.php?ID="+id, true);
  xmlhttp.send();
}

function showReceived(){
  $("#ordiniC").fadeToggle();
}

function getNext(){
  count = next();
  show();
}

function getPrev(){
  count = prev();
  show();
}

function next(){
  return count == (length-1) ? 0 : count + 1;
}

function prev(){
  return count == 0 ? (length-1) : count - 1;
}

function show(){
  $("#p").attr("src", ar_f[prev()][c_im]);
  $("#c").attr("src", ar_f[count][c_im]);
  $("#n").attr("src", ar_f[next()][c_im]);
  $(".desc > h3").html(ar_f[count][1]);
  $("#showIm > img").attr("src", ar_f[count][c_im]);
  $("#description").html(ar_f[count][3]);
  $("#ing").html(ar_f[count][2]);
  $("#price").html("$" +  ar_f[count][4]);
}

function addFood(){
  var prodotto = ar_f[count][0];
  var categoria= ar_f[count][6];
  var xmlhttp;
  xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "personalPage.php?prodotto="+prodotto+"&categoria="+categoria, true);
  xmlhttp.send();
}

function openCart(){
  window.open("../cartPage/cartPage.php");
}
/*
function logOut(){
  window.open("../welcome/welcome.php");
}*/
