<?php
  $q_d = "SELECT * FROM sconti";
  $discount_res = $conn->query($q_d);
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="discount.css">
    <title>Discounts</title>
  </head>
  <body>
    <div id="discounts">
      <h1>Sconti Attuali</h1>
      <?php
      if($discount_res->num_rows > 0){
        while($row = $discount_res->fetch_assoc()){
          $idc = $row['Categoria'];
          $f_cn = "SELECT Nome AS name FROM Categorie WHERE ID='$idc'";
          $res_cn = $conn->query($f_cn);
          $res_cn1 = mysqli_fetch_array($res_cn);
          $cat_name = $res_cn1['name'];

          ?>
          <p>Tutti i  prodotti di tipo "<?php echo $cat_name ?>" sono scontati del <?php echo $row['Sconto'] . "%"?></p>
          <?php
        }
      }
        ?>
    </div>
  </body>
</html>
