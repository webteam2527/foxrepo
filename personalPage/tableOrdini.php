<?php

 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>tableOrdini</title>
   </head>
   <body>
     <div id="ordiniC">
       <h1> I Tuoi Ordini Confermati</h1>
     <table>
     <thead>
       <tr>
       <th id="number">NumeroOrdine</th>
       <th id="prod">ProdottiOrdinati</th>
       <th id="tot">Totale</th>
       <th id="ad">Indirizzo</th>
       <th></th>
      </tr>
     </thead>
     <tbody>
        <?php
          if($ordine_res->num_rows > 0){
            while($row = $ordine_res->fetch_assoc()){?>
              <tr>
              <td headers="number"><?php echo $row['NumeroOrdine'] ?></td>
              <td headers="prod"><?php echo $row['ProdottiOrdinati'] ?></td>
              <td headers="tot"> € <?php echo number_format($row['Totale'],2,".",",") ?></td>
              <td headers="ad"><?php echo $row['Indirizzo'] ?></td>
              <td><input type="button" name="ricevuto" value="ricevuto" onclick="ordineRicevuto(<?php echo $row['ID'] ?>)"></td>
              </tr>
              <?php
            }
          }
         ?>
     </tbody>
   </table>
 </div>
  <?php
  if(isset($_GET['ID'])){
      $id = $_GET['ID'];
      $del_q = "DELETE FROM ordiniC WHERE ID = '$id' ";
      $del_res = $conn->query($del_q);
  }
   ?>
   </body>
 </html>
