<?php
include('verify.php');
$price = $_SESSION['price'];
 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PaymentPage</title>
    <link rel="stylesheet" href="payment_css.css">
    <script src="jquery-3.2.1.min.js"></script>
  </head>
  <body>
    <header>
      <span>Great, that's </span><span>$ <?php echo $price ?></span>
    </header>
    <?php
    $mes = "";
    if(isset($_GET['m'])){
      $mes = $_GET['m'];
    }
     ?>
    <div>
      <?php echo $mes; ?>
    </div>
    <section>
      <h1 hidden>titolo</h1>
      <form class="" action="verify.php" method="post">
      <fieldset class="card">
        <span>Selezionare il tipo di pagamento</span>
        <div id="main">
          <div>
              <input id="paypal" type="radio" name="payment" value="PayPal">
              <label for="paypal"></label>
              <img src="paypal.png" alt="Not Available" width="40" height="40">
          </div>
          <div>
              <input id="mastercard" type="radio" name="payment" value="Mastercard">
              <label for="mastercard"></label>
              <img src="mastercard.png" alt="Not Available" width="40" height="40">
          </div>
          <div>
            <input id="visa" type="radio" name="payment" value="Visa">
            <label for="visa"></label>
            <img src="visa.png" alt="Not Available" width="40" height="40">
          </div>
        </div>
        </fieldset>
        <fieldset id="cc">
            <label for="oc">CardOwner</label><br>
            <input id="oc" type="text" name="owner" value=" "><br>
            <label for="ad">Delivery Address</label><br>
            <input id="ad" type="text" name="ad" value=" "><br>
            <label for="cn">Card Number</label><br>
            <input id="cn" type="text" name="cardNum" value=" "><br>
            <label for="ex">Expire Date</label><br>
            <input id="ex" type="date" name="date" value=" "><br>
            <label for="cvc">CVV</label><br>
            <input id="cvc" type="text" name="cvv"><br>
        </fieldset>
        <input id="sub" type="submit" name="pay" value="Done!">
      </form>
    </section>
  </body>
</html>
