<?php
include('../shared/conn.php');
  if(isset($_POST['pay'])){
    if(isset($_POST['payment']) && isset($_POST['owner']) && isset($_POST['ad']) && isset($_POST['cardNum']) && isset($_POST['date']) && isset($_POST['cvv'])){
      $payment = $_POST['payment'];
      $owner = $_POST['owner'];
      $address = $_POST['ad'];
      $_SESSION['Address'] = $address;
      $numCard = $_POST['cardNum'];
      $date = $_POST['date'];
      $date1 = strtotime($date);
      $cvv = $_POST['cvv'];

      $todayDate =  date('Y/m/d');
       $td = strtotime($todayDate);


        if(empty($owner) || empty($payment) || empty($address) || empty($numCard) || empty($date1)|| empty($cvv)){
          $message ='Compilare tutti i campi!';
          header("location: payment.php?m=". $message);
        } else if(strlen($numCard) != '16'){
          $message = 'Numero carta inserito non valido!';
          header("location: payment.php?m=". $message);
        } else if(strlen($cvv) != '3'){
          $message = 'CVV inserito non valido!';
          header("location: payment.php?m=" . $message);
        }else if($date1 < $td){
          $message = 'Carta scaduta, impossibile pagamento!';
          header("location: payment.php?m=" .$message);
        }else if(!isset($_POST['payment'])){
          $message = 'Nessun pagamento selezionato!';
          header("location: payment.php?m=". $message);
        }else if(empty($address)){
          $message = 'Indirizzo di consegna non specificato!';
          header("location: payment.php?m=". $message);
        }
      }else{
        $message = 'Compilare tutti i campi!';
        header("location: payment.php?m=". $message);
      }
      ?>
     <!DOCTYPE html>
      <html>
        <head>
          <meta charset="utf-8">
          <title>payment</title>
          <script src="jquery-3.2.1.min.js"></script>
          <script src="payment.js"></script>
          <style>
            body{
              background-color: rgb(147, 221, 129);
              font-size: 100%;
            }
            section{
              display: flex;
              flex-direction: column;
              align-items: center;
              justify-content: center;
            }
            label,span{
              font-size: 10pt;
            }
          </style>
        </head>
        <body>
          <section>
            <h2>Ricevuta Pagamento</h2>
            <div>
              <label>Acquisto fatto da:</label>
              <span><?php echo $_SESSION['Username'] ?></span> <br>
              <label>Acquisto effettuato con:</label>
              <span><?php echo $payment ?></span><br>
              <label>Numero carta:</label>
              <span ><?php echo $numCard ?></span><br>
              <label>Titolare carta:</label>
              <span><?php echo $owner ?></span><br>
              <label>Indirizzo di Consegna:</label>
              <span><?php echo $address ?></span><br>
              <label>Totale acquisto:</label>
              <span> € <?php echo  number_format($_SESSION['price'] ,2,".",",") ?></span>
            </div>
            <input id="done" type="button" name="done" value="OK" onclick="insertOrder()">
          </section>
        </body>
      </html>
<?php
  }
  $user = $_SESSION['Username'];
  $total = $_SESSION['price'];
  $norder_q = "SELECT MAX(NumeroOrdine) AS max from ordiniS WHERE User='$user'";
  $res_norder = $conn->query($norder_q);
  $get_norder = mysqli_fetch_array($res_norder);
  $max = $get_norder['max'];
  if($max == null){
    $max = 1;
  }else{
    $max = $max + 1;
  }

  $prod_q = "SELECT Prodotto FROM Ordini WHERE User = '$user'";
  $res_prod = $conn->query($prod_q);
  $v1 = array();
  $str = null;
  if($res_prod->num_rows > 0){
    while($row = $res_prod->fetch_assoc()){
      foreach ($row as $v => $x) {
        $n_q = "SELECT Nome AS name FROM Prodotti WHERE ID=$x";
        $res_name = $conn->query($n_q);
        $get_name= mysqli_fetch_array($res_name);
        $name = $get_name['name'];
        $space = ";   ";
        $str .= $name . $space;
      }
    }
  }

  if(isset($_GET['DONE'])){
    if($_GET['DONE'] == "OK"){
        $addr = $_SESSION['Address'];
        $i_q = "INSERT INTO ordiniS (User, NumeroOrdine, ProdottiOrdinati, Totale, Indirizzo) VALUES ('$user', '$max', '$str', '$total', '$addr')";
        $d_q = "DELETE FROM Ordini WHERE User='$user'";
        $conn->query($i_q);
        $conn->query($d_q);
        header("location: ../personalPage/personalPage.php");
     }
  }
 ?>
