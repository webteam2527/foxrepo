<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Header_C</title>
    <link rel="stylesheet" href="../shared/header_client.css">
  </head>
  <body>
    <div id="head">
        <div>
          <figure>
            <img src="../welcome/fox_icon.png" alt="">
            <span>Fox</span><span>Eat</span>
          </figure>
          <nav>
            <ul>
              <li><a href="../informations/aboutUs.php">Chi Siamo</a></li>
              <li><a href="../informations/findUs.php">Dove Siamo</a></li>
              <li><a href="../informations/contactUs.php">Contatti</a></li>
            </ul>
          </nav>
        </div>
        <div>
          <nav>
            <ul>
              <li id="home"><img src="../images/home.png" alt="X"><a href="../home/homePage.php">Home</a></li>
              <li id="cart"><img src="../images/cart.png" alt="X"><a href="../cartPage/cartPage.php">Cart</a></li>
              <li id="logout"><img src="../images/logout.ico" alt="X"><a href="../welcome/welcome.php">Leave</a></li>
              <li id="signup"><img src="../images/signUp.png" alt="X"><a href="../signUpPage/signUpPage.php">Sign Up</a></li>
              <li id="login"><img src="../images/user-icon.png" alt="X"><a href="../welcome/welcome.php">Log In</a></li>
              <li id="admin"><img src="../images/admin.png" alt="X"><a href="../admin/welcomeAdmin.php">Admin</a></li>
              <li id="back"><img src="../images/back.png" alt="X"><a href="../personalPage/personalPage.php?c_select=Pizza">Back</a></li>
            </ul>
          </nav>
        </div>
    </div>
  </body>
</html>
