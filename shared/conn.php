<?php
session_start();
$servername = 'localhost';
$username = 'root';
$password = "";

$conn = new mysqli($servername, $username, $password);

$create_db = "CREATE DATABASE  IF NOT EXISTS foxeatdatabase";

if($conn->query($create_db) == TRUE){
  //echo "DB creato correttamente";
} else {
  echo "Errore nella creazione del DB" . $conn->error;
}

$conn = new mysqli($servername, $username, $password ,"foxeatdatabase");

$admin_tb = "CREATE TABLE IF NOT EXISTS admin (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             name TEXT NOT NULL,
                             surname TEXT NOT NULL,
                             password TEXT NOT NULL)";

if($conn->query($admin_tb) == TRUE){
  //echo "admin table creata correttamente";
}else{
  echo "Errore" . $admin_tb . "<br>" .$conn->error;
}

$admin_ins = "INSERT INTO admin (name, surname, password) VALUES('defaultName', 'defaultSurname', 'defaultPassword')";
$conn->query($admin_ins);

$categ_tb = "CREATE TABLE IF NOT EXISTS Categorie (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             nome TEXT NOT NULL)";

if($conn->query($categ_tb) == TRUE){
  //echo "tabella categoria creata correttamente";
}else{
  echo "Errore" . $categ_tb . "<br>" .$conn->error;
}

$news_tb = "CREATE TABLE IF NOT EXISTS news (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             nome TEXT NOT NULL,
                             descrizione TEXT NOT NULL,
                             data DATE NOT NULL,
                              path TEXT NOT NULL)";

if($conn->query($news_tb) == TRUE){
  //echo "news table creata correttamente";
}else{
  echo "Errore" . $news_tb . "<br>" .$conn->error;
}

$newsCliente_tb = "CREATE TABLE IF NOT EXISTS newsC (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             news int NOT NULL,
                             user TEXT NOT NULL)";

if($conn->query($newsCliente_tb) == TRUE){
  //echo "news cliente table creata correttamente";
}else{
  echo "Errore" . $newsCliente_tb . "<br>" .$conn->error;
}

$ordini_tb = "CREATE TABLE IF NOT EXISTS Ordini (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             user TEXT NOT NULL,
                             prodotto int NOT NULL,
                             categoria int NOT NULL)";

if($conn->query($ordini_tb) == TRUE){
  //echo "ordini table creata correttamente";
}else{
  echo "Errore" . $ordini_tb . "<br>" .$conn->error;
}

$ordiniConfermati_tb = "CREATE TABLE IF NOT EXISTS ordiniC (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             user TEXT NOT NULL,
                             numeroordine int NOT NULL,
                             prodottiordinati TEXT NOT NULL,
                              totale TEXT NOT NULL,
                              indirizzo TEXT NOT NULL)";

if($conn->query($ordiniConfermati_tb) == TRUE){
  //echo "ordini confermati table creata correttamente";
}else{
  echo "Errore" . $ordiniConfermati_tb . "<br>" .$conn->error;
}

$ordiniSospesi_tb = "CREATE TABLE IF NOT EXISTS ordiniS (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             user TEXT NOT NULL,
                             numeroordine int NOT NULL,
                             prodottiordinati TEXT NOT NULL,
                              totale TEXT NOT NULL,
                              indirizzo TEXT NOT NULL)";

if($conn->query($ordiniSospesi_tb) == TRUE){
  //echo "ordini in sospeso table creata correttamente";
}else{
  echo "Errore" . $ordiniSospesi_tb . "<br>" .$conn->error;
}

$prodotti_tb = "CREATE TABLE IF NOT EXISTS Prodotti (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             nome TEXT NOT NULL,
                             ingredienti TEXT NOT NULL,
                             descrizione TEXT NOT NULL,
                              prezzo TEXT NOT NULL,
                              path TEXT NOT NULL,
                              categoria int NOT NULL)";

if($conn->query($prodotti_tb) == TRUE){
  //echo "prodotti table creata correttamente";
}else{
  echo "Errore" . $prodotti_tb . "<br>" .$conn->error;
}

$sconti_tb = "CREATE TABLE IF NOT EXISTS sconti (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             categoria int NOT NULL,
                             sconto int NOT NULL)";

if($conn->query($sconti_tb) == TRUE){
  //echo "sconto table creata correttamente";
}else{
  echo "Errore" . $sconti_tb . "<br>" .$conn->error;
}

$scontiCliente_tb = "CREATE TABLE IF NOT EXISTS scontiC (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             sconto int NOT NULL,
                             user TEXT NOT NULL)";

if($conn->query($scontiCliente_tb) == TRUE){
  //echo "sconti cliente table creata correttamente";
}else{
  echo "Errore" . $scontiCliente_tb . "<br>" .$conn->error;
}

$user_tb = "CREATE TABLE IF NOT EXISTS Users (
                             ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                             name TEXT NOT NULL,
                             surname TEXT NOT NULL,
                             eta int NOT NULL,
                             genere VARCHAR(1) NOT NULL,
                             indirizzo TEXT NOT NULL,
                             telefono TEXT NOT NULL,
                             email TEXT NOT NULL,
                             username TEXT NOT NULL,
                             password TEXT NOT NULL)";

if($conn->query($user_tb) == TRUE){
  //echo "user table creata correttamente";
}else{
  echo "Errore" . $user_tb . "<br>" .$conn->error;
}

?>
