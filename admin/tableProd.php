<?php
  include("../shared/conn.php");
  $p_q = "SELECT * FROM Prodotti ORDER BY Categoria";
  $res_p = $conn->query($p_q);

 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Prodotti</title>
    <link rel="stylesheet" href="tableProd.css">
    <script src="jquery-3.2.1.min.js"></script>
    <script src="tableProd.js"></script>
  </head>
  <body>
    <header>
      <nav>
        <a href="adminChoice.php">Scelta Operazione</a>
      </nav>
    </header>
    <table>
      <caption>Prodotti in Menu</caption>
      <thead>
        <th id="nome">Nome</th>
        <th id="price">Prezzo</th>
        <th id="cat">Categoria</th>
        <th id="emp"></th>
      </thead>
      <tbody>
        <?php
          if($res_p->num_rows > 0){
            while($row = $res_p->fetch_assoc()){
              $id = $row['ID'];
              $name = $row['Nome'];
              $price = $row['Prezzo'];
              $categoria = $row['Categoria'];
              $c_q = "SELECT Nome AS n FROM Categorie WHERE ID='$categoria'";
              $res_cn = $conn->query($c_q);
              $res_cn1 = mysqli_fetch_array($res_cn);
              $cname = $res_cn1['n'];
              ?>
              <tr id="<?php echo $id ?>">
                <td headers="nome"><?php echo $name ?></td>
                <td headers="price">€ <?php echo $price ?></td>
                <td headers="cat"><?php echo $cname ?></td>
                <td><input type="button" name="" value="Elimina" onclick="deleteProd(<?php echo $id ?>)"></td>
              </tr>
              <?php
            }
          }
         ?>
      </tbody>
    </table>
    <?php
      if(isset($_GET['id'])){
        $idp = $_GET['id'];
        $n_p = "SELECT Nome AS namep From Prodotti WHERE ID='$idp'";
        $dp_q = "DELETE FROM Prodotti WHERE ID='$idp'";
        $res_pn = $conn->query($n_p);
        $res_pn1 = mysqli_fetch_array($res_pn);
        $name_p = $res_pn1['namep'];
        $dn_q = "DELETE FROM news   WHERE Nome='$name_p'";
        $conn->query($dn_q);
        $conn->query($dp_q);
      }
     ?>
  </body>
</html>
