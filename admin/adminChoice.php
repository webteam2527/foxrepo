<?php
include('../shared/conn.php');

  $tot_q = "SELECT COUNT(ID) as tot FROM ordiniS";
  $res_t = $conn->query($tot_q);
  $res_order = mysqli_fetch_array($res_t);
  $totOrders = $res_order['tot'];

  $news_q = "SELECT * FROM news";
  $news_res = $conn->query($news_q);
 ?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>adminChoice</title>
    <link rel="stylesheet" href="adminChoice.css">
    <script  src="jquery-3.2.1.min.js"></script>
    <script  src="admin.js"></script>
    <script  src="confermOrder.js"></script>
  </head>
  <body>
    <section>
      <h1>Scelta Operazione</h1>
      <p>Ordini da confermare :<label><?php echo $totOrders ?></label></p>
      <div id="gestione">
      <nav>
        <ul>
          <li><a href="tableProd.php">Elimina Prodotti</a></li>
          <li><a href="signAdmin.php">Registra Amministratore</a></li>
          <li><a href="tableAdmin.php">Aggiungi prodotti</a></li>
          <li><a href="discount.php">Aggiungi sconto</a></li>
          <li><a href="confirmOrder.php">Conferma ordini</a></li>
        </ul>
      </nav>
        <div>
          <label for="addNews"></label>
          <input id="addNews" type="button" name="addNews" value="Gestisci news" onclick="showAllNews()">
          <label for="addSconto"></label>
          <input id="addSconto" type="button" name="addSconto" value="Gestisci sconti" onclick="showAllDiscounts()">
        </div>
      </div>
    </section>
    <?php include("tableNews.php") ?>
    <?php include("tableDiscount.php"); ?>
  </body>
</html>
