<?php
  include('../shared/conn.php');
 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Discount</title>
    <script src="jquery-3.2.1.min.js"></script>
    <script src="../admin/discount.js"></script>
    <link rel="stylesheet" href="discount.css">
  </head>
  <body>
    <header>
      <nav>
      <a href="adminChoice.php">Scelta Operazione</a>
     </nav>
    </header>
    <div id="discount">
      <form action="adminChoice.php" method="get">
        <fieldset>
        <legend>Applica nuovo sconto su categoria</legend>
          <label>
            Categoria:
            <select id="category" name="sceltaCat">
              <option value="dolci">Dolci</option>
              <option value="hamburger">Hamburger</option>
              <option value="pizza">Pizza</option>
              <option value="sandwitch">Sandwitch</option>
              <option value="insalate">Insalate</option>
              <option value="Hot Dog">Hot Dog</option>
              <option value="burrito">Burrito</option>
              <option value="bevande">Bevande</option>
              <option value="fritti">Fritti</option>
            </select>
          </label> <br>
          <label for="sconto">
            Sconto da applicare:
            <input id="sconto" type="number" name="sconto" value="" min="10" max="50">
            %
          </label><br>
        <input id="apply" type="button" value="Applica">
      </fieldset>
      </form>
    </div>
  </body>
  <?php
    if(isset($_GET['c']) && isset($_GET['d'])){
      $cat = $_GET['c'];
      $dis = $_GET['d'];

      $c_q = "SELECT ID AS id FROM Categorie WHERE Nome = '$cat'";
      $res_c = $conn->query($c_q);
      $res_c1 = mysqli_fetch_array($res_c);
      $cat_id = $res_c1['id'];

      $i_q = "INSERT INTO sconti (Categoria, Sconto) VALUES ('$cat_id', '$dis')";
      $conn->query($i_q);
    }

   ?>
</html>
