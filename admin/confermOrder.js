$(document).ready(function(){
  $("#tableNews").hide();
  $("#tableDiscount").hide();

});

function confirm(id){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "confirmOrder.php?id="+id, true);
  xmlhttp.send();

}

function showAllNews(){
  $("#tableNews").fadeToggle();
}

function showAllDiscounts(){
  $("#tableDiscount").fadeToggle();
}

function deleteNews(id){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "adminChoice.php?id="+id, true);
  xmlhttp.send();
}

function deleteDiscount(id){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "adminChoice.php?idd="+id, true);
  xmlhttp.send();
}
