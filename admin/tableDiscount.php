<?php
  $q_d = "SELECT * FROM sconti";
  $discount_res = $conn->query($q_d);
 ?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>DiscountTable</title>
   </head>
   <body>
     <div id="tableDiscount">
       <h1>Sconti Attuali</h1>
     <table>
       <thead>
         <tr>
         <th>Categoria</th>
         <th>Sconto Applicato</th>
         <th>Cancella Sconto</th>
       </tr>
       </thead>
       <tbody>
         <?php
         if($discount_res->num_rows > 0){
           while($row = $discount_res->fetch_assoc()){
             $idc = $row['Categoria'];
             $f_cn = "SELECT Nome AS name FROM Categorie WHERE ID='$idc'";
             $res_cn = $conn->query($f_cn);
             $res_cn1 = mysqli_fetch_array($res_cn);
             $cat_name = $res_cn1['name'];
             ?>
             <tr>
               <td><?php echo $cat_name ?></td>
               <td><?php echo $row['Sconto'] . "%" ?></td>
               <td><input type="button" name="delete" value="Elimina" onclick="deleteDiscount(<?php echo $row['ID'] ?>)"></td>
             </tr>
             <?php
           }
         }
           ?>
       </tbody>
     </table>
   </div>
   <?php
    if(isset($_GET['idd'])){
      $idd = $_GET['idd'];
      $del_dis = "DELETE FROM sconti WHERE ID = '$idd'";
      $del_c_dis = "DELETE FROM scontiC WHERE Sconto = '$idd'";
      $conn->query($del_dis);
      $conn->query($del_c_dis);
    }
    ?>
   </body>
 </html>
