<?php

 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>tableNews</title>
     <link rel="stylesheet" href="tableNews.css">
   </head>
   <body>
     <div id="tableNews">
       <h1>News Attuali</h1>
     <table>
       <thead>
         <tr>
         <th>Nome</th>
         <th>Aggiunta il</th>
         <th>Elimina News</th>
       </tr>
       </thead>
       <tbody>
         <?php
         if($news_res->num_rows > 0){
           while($row = $news_res->fetch_assoc()){?>
             <tr>
               <td><?php echo $row['Nome']?></td>
               <td><?php echo $row['Data']?></td>
               <td><input type="button" name="delete" value="elimina" onclick="deleteNews(<?php echo $row['ID'] ?>)"></td>
             </tr>
             <?php
           }
         }
           ?>
       </tbody>
     </table>
   </div>
   <?php
    if(isset($_GET['id'])){
      $id = $_GET['id'];
      $del_news = "DELETE FROM news WHERE ID = '$id'";
      $del_c_news = "DELETE FROM newsC WHERE News = '$id'";
      $conn->query($del_news);
      $conn->query($del_c_news);
    }
    ?>
   </body>
 </html>
