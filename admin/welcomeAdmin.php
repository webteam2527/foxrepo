<?php
include('loginAdmin.php');
 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>welcomeAdmin</title>
    <link rel="stylesheet" href="admin.css">
  </head>
  <body>
    <section>
    <h1>Login Amministratore</h1>
    <form class="" action="loginAdmin.php" method="post">
      <label for="adminName">Admin name:</label><br/>
      <input id="adminName" type="text" name="adminName" value=""><br/>
      <label for="adminSurname">Admin surname:</label><br/>
      <input id="adminSurname" type="text" name="adminSurname" value=""><br/>
      <label for="adminPassword">Admin password:</label><br/>
      <input id="adminPassword" type="password" name="adminPassword" value=""><br/>
      <input type="submit" name="login" value="Log in!">
    </form>
  </section>
  </body>
</html>
