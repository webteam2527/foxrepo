<?php
include('../shared/conn.php');
$sql_o = "SELECT * FROM ordiniS";
$sql = $conn->query($sql_o);
 ?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>ConfirmOrder</title>
     <link rel="stylesheet" href="admin.css">
     <link rel="stylesheet" href="confirmOrder.css">
     <script  src="jquery-3.2.1.min.js"></script>
     <script  src="confermOrder.js"></script>
   </head>
   <body>
     <header>
       <nav>
       <a href="adminChoice.php">Scelta Operazione</a>
     </nav>
     </header>
     <div>
       <h1>Ordini da Confermare</h1>
     <table>
       <thead>
         <tr>
         <th id="user">User</th>
         <th id="number">NumeroOrdine</th>
         <th id="prod">ProdottiOrdinati</th>
         <th id="tot">Totale €</th>
         <th id="addr">Indirizzo</th>
         <th id="conf"></th>
        </tr>
       </thead>
       <tbody>
          <?php
            if($sql->num_rows > 0){
              while($row = $sql->fetch_assoc()){?>
                <tr>
                  <td headers="user"><?php echo $row['User']?></td>
                  <td headers="number"><?php echo $row['NumeroOrdine']?></td>
                  <td headers="prod"><?php echo $row['ProdottiOrdinati']?></td>
                  <td headers="tot"><?php echo number_format($row['Totale'],2,".",",")?></td>
                  <td headers="addr"><?php echo $row['Indirizzo'] ?></td>
                  <td headers="conf"><input type="button" name="conferma" value="Conferma" onclick="confirm(<?php echo $row['ID']?>)"></td>
                </tr>
          <?php
              }
            }
          ?>
          <?php
          if(isset($_GET['id'])){
            $id = $_GET['id'];
            $q_id = "SELECT * FROM ordiniS WHERE ID = '$id' ";
            $res = $conn->query($q_id);
            $values = $res->fetch_array();
            $user =   $values['User'];
            $numOrdini = $values['NumeroOrdine'];
            $pOrdinati = $values['ProdottiOrdinati'];
            $tot = $values['Totale'];
            $ad = $values['Indirizzo'];
            $in_1 = "INSERT INTO ordiniC (User, NumeroOrdine, ProdottiOrdinati,Totale, Indirizzo) VALUES (
                                          '$user',
                                          '$numOrdini',
                                          '$pOrdinati',
                                          '$tot',
                                          '$ad')";
            $del = "DELETE FROM ordiniS WHERE ID='$id'";
            $conn->query($in_1);
            $conn->query($del);
          }
          ?>
       </tbody>
     </table>
   </div>
   </body>
 </html>
