<?php
include('../shared/conn.php');

if(isset($_POST['insert'])){
  if(isset($_POST['prodName']) && isset($_POST['prodIng']) && isset($_POST['prodDesc']) && isset($_POST['prodPrice']) &&
  isset($_POST['prodPath']) && isset($_POST['prodCat'])){
    $prodName = $_POST['prodName'];
    $prodIng = $_POST['prodIng'];
    $prodDesc = $_POST['prodDesc'];
    $prodPrice = $_POST['prodPrice'];
    $prodPath = $_POST['prodPath'];
    $prodCat = $_POST['prodCat'];

    $sql_c = "SELECT ID AS id FROM Categorie WHERE Nome = '$prodCat'";
    $res_c = $conn->query($sql_c);
    $id = mysqli_fetch_array($res_c);
    $id_cat = $id['id'];
    $prodPrice = str_replace(",",".",$prodPrice);
    $sql_p = "INSERT INTO Prodotti (Nome, Ingredienti, Descrizione, Prezzo, Path, Categoria) VALUES (
                                    '$prodName',
                                    '$prodIng',
                                    '$prodDesc',
                                    '$prodPrice',
                                    '$prodPath',
                                    '$id_cat')";

    $result_p = $conn->query($sql_p);
    $todayDate =  date('d/m/Y');
    $sql_n = "INSERT INTO news (Nome, Descrizione, Data, Path) VALUES (
                                    '$prodName',
                                    '$prodDesc',
                                    '$todayDate',
                                    '$prodPath')";

    $result_n = $conn->query($sql_n);
  }
}
  ?>

  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title>InsertProduct</title>
      <link rel="stylesheet" href="admin.css">
    </head>
    <body>
      <header>
        <nav>
            <a href="adminChoice.php">Scelta operazione</a>
        </nav>
      </header>
      <section>
        <h1>Aggiungi nuovi Piatti:</h1>
      <form action="tableAdmin.php" method="post">
        <label for="prodName">Nome prodotto:</label>
        <input id="prodName" type="text" name="prodName" value="" required><br>
        <label for="prodIng">Ingredienti:</label>
        <input id="prodIng" type="text" name="prodIng" value="" required><br>
        <label for="prodDesc">Descrizione:</label>
        <input id="prodDesc" type="text" name="prodDesc" value="" required><br>
        <label for="prodPrice">Prezzo:</label>
        <input id="prodPrice" type="text" name="prodPrice" value="" required><br>
        <label for="prodPath">Path:</label>
        <input id="prodPath" type="text" name="prodPath" value="" required><br>
        <label for="prodCat">Categoria:</label>
        <input id="prodCat" type="text" name="prodCat" value="" required><br>
        <input type="submit" name="insert" value="Inserisci Prodotto" required>
    </form>
  </section>
    </body>
  </html>
