
$(document).ready(function(){
  $("#pay").click(openPayment);

  $("#admin").hide();
  $("#login").hide();
  $("#cart").hide();
  $("#signup").hide();


});
function addProduct(idProd, p){
    console.log(p);
   $("#"+idProd+" .quantity").html(increment(idProd, p));
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.open("GET", "cartPage.php?id="+idProd+"&op=Add", true);
   xmlhttp.send();
}

function canProduct(idProd, p){
  $("#"+idProd+" .quantity").html(decrement(idProd, p));
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "cartPage.php?id="+idProd+"&op=Can", true);
  xmlhttp.send();
}

function delProduct(idProd, p){
  var qt = parseInt($("#"+idProd+" .quantity").html());
  console.log(qt);

  var actualSubPrice = $("#subt").html();
  var actualTot = $("#totp").html();
  var nsub = parseFloat(actualSubPrice) - parseFloat(p)*qt;
  var ntot = parseFloat(actualTot) - parseFloat(p)*qt;
  $("#subt").html(nsub.toFixed(2));
  $("#totp").html(ntot.toFixed(2));
  $("#"+idProd+" .quantity").html(0);
  $("#"+idProd).fadeOut();
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "cartPage.php?id="+idProd+"&op=Del", true);
  xmlhttp.send();
}

function increment(idProd, p){
  var actualSubPrice = $("#subt").html();
  var actualTot = $("#totp").html();
  var nsub = parseFloat(actualSubPrice) + parseFloat(p);
  var ntot = parseFloat(actualTot) + parseFloat(p);
  $("#subt").html(nsub.toFixed(2));
  $("#totp").html(ntot.toFixed(2));

  var actualQuantity = $("#"+idProd+" .quantity").html();
  return parseInt(actualQuantity)+1;
}

function decrement(idProd, p){
  var actualSubPrice = $("#subt").html();
  var actualTot = $("#totp").html();
  var nsub = parseFloat(actualSubPrice) - parseFloat(p);
  var ntot = parseFloat(actualTot) - parseFloat(p);
  $("#subt").html(nsub.toFixed(2));
  $("#totp").html(ntot.toFixed(2));

  var actualQuantity = $("#"+idProd+" .quantity").html();
  if(actualQuantity === "1"){
    $("#"+idProd).fadeOut();
  }
  return parseInt(actualQuantity) == 0 ? 0 : parseInt(actualQuantity) - 1;
}

function openPayment(){
  var st = parseInt($("#subt").html());
  if(st > 0){
    window.open('../payment/payment.php');
  }else{
    $("#error > label").html("Nessun elemento presente nel carrello!");
  }
}
