<?php
  include('../shared/conn.php');
//  echo "BENVENUTO : " . $_SESSION['Username'];
  $user = $_SESSION['Username'];
  $getFood = "SELECT DISTINCT Prodotto FROM Ordini WHERE User='$user'";
  $resultGF = $conn->query($getFood);
 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>cartPage</title>
    <link rel="stylesheet" href="cartPage.css">
    <script src="jquery-3.2.1.min.js"></script>
    <script src="cart.js"></script>
  </head>
    <body>
      <header>
        <?php include("../shared/header_client.php") ?>
        <h1>Il tuo carrello</h1>
      </header>
      <section id="tab">
        <h2 hidden>Tabella Carrello</h2>
        <table>
          <thead>
            <tr>
              <th id="empty"></th>
              <th id="nome">Nome</th>
              <th id="p">Prezzo B.</th>
              <th id="s">Sconto</th>
              <th id="ps">Prezzo S.</th>
              <th id="q">Qtà</th>
              <th id="pl"></th>
              <th id="min"></th>
              <th id="del"></th>
            </tr>
          </thead>
          <tbody>
          <?php
              $total = 0.00;
      				if ($resultGF->num_rows > 0) {
      					while($row = $resultGF->fetch_assoc()) {
                  $identifier = $row["Prodotto"]; /* PROD IDENTIFIER ORDERED BY USER */
                  $user = $_SESSION['Username'];  /* USERNAME */
                  /*$qp = "SELECT *
                         FROM Ordini
                         WHERE ID=(SELECT DISTINCT Prodotto
                                    FROM Ordini
                                    WHERE User= '$user')";*/
                  $qp = "SELECT * FROM Prodotti WHERE ID='$identifier'";  /* WORKS PROPERLY */

                  $res_p = $conn->query($qp);
                  $instance_p = $res_p->fetch_assoc();
                  $imm = $instance_p["Path"];
                  $nome = $instance_p["Nome"];
                  $price = $instance_p["Prezzo"];
                  $cat = $instance_p["Categoria"];

                  $f_d = "SELECT Sconto AS d FROM sconti WHERE Categoria = '$cat'";
                  $res_d = $conn->query($f_d);
                  $res_d1 = mysqli_fetch_array($res_d);
                  $dis = $res_d1['d'];
                  $price_s = $price;
                  if($dis != null){
                    $price_s = $price - ($price*$dis/100);
                  }
                  $qq = "SELECT COUNT(ID) AS qta FROM Ordini WHERE Prodotto='$identifier' AND User='$user'";
                  $res_qq = $conn->query($qq);
                  $qta = mysqli_fetch_array($res_qq);
                  $quantity = $qta['qta'];
                  $total += $quantity*$price_s;
      						?>
      						<tr id="<?php echo $identifier ?>">
                    <td class="image"><img src=<?php echo $imm ?> alt="Not Available" width="50px" height="50px;"></td>
      							<td class="name"><?php echo $nome; ?></td>
                    <td class="prezzoB"><?php echo "€ " . $price ?></td>
                    <td class="sconto"><?php
                      if($dis != null){
                        echo $dis . "%";
                      }
                     ?></td>
                     <td class="prezzoS"><?php echo "€" . number_format($price_s,2,".",",") ?></td>
                    <td class="quantity"><?php echo $quantity ?></td>
                    <td class="plus"><label for="plus"></label><input id="plus" title="Aggiungi" type="button" value=" " onclick="addProduct(<?php echo $identifier ?>,<?php echo $price_s ?>)"></td>
                    <td class="minus"><label for="minus"></label><input id="minus" title="Togli" type="button" value=" " onclick="canProduct(<?php echo $identifier ?>, <?php echo $price_s ?>)"></td>
                    <td class="rem"><label for="delete"></label><input id="delete" title="Elimina" type="button" value=" " onclick="delProduct(<?php echo $identifier ?>, <?php echo $price_s ?>)"></td>
      						</tr>
      						<?php
      					}
      				}
			     ?>
        </tbody>
        </table>
      </section>
      <section id="resume">
          <h2 hidden>Riepilogo</h2>
          <?php
            $delivery = 2;
            $totalDel = $total + $delivery;
            $_SESSION['price'] = $totalDel;
           ?>
          <div><label>Sub Totale: € <label id="subt"><?php echo number_format($total,2,".",",") ?></label></label></div>
          <div><label>Spedizione: €  <?php echo $delivery ?>.00</label></div>
          <div><label>Totale: € <label id="totp"> <?php echo number_format($totalDel,2,".",",")?></label></label></div>
      </section>
      <section id="payment">
        <h2 hidden>Pagamento</h2>
        <input id="pay" type="button" value="Proceed">
      </section>
      <div id="error">
        <label></label>
      </div>
      <?php
       if(isset($_GET['id']) && isset($_GET['op'])){
         $id = $_GET['id'];
         $op = $_GET['op'];
         $qid = "SELECT * FROM Prodotti WHERE ID='$id'";
         $p = $conn->query($qid);
         $i_p = $p->fetch_assoc();
         $cat = $i_p["Categoria"];
         $name = $i_p["Nome"];
         if($op == "Add"){
           $insert_q = "INSERT INTO Ordini (User, Prodotto, Categoria) VALUES ('$user', '$id', '$cat')";
           $conn->query($insert_q);
         }else if ($op == "Can"){
           $can_p = "SELECT MAX(ID) AS toCan FROM Ordini WHERE Prodotto='$id' AND User='$user'";
           $res_d = $conn->query($can_p);
           $res_d2 = mysqli_fetch_array($res_d);
           $did = $res_d2['toCan'];
           $can_q = "DELETE FROM Ordini WHERE ID='$did'";
           $conn->query($can_q);
         }else{
           $del_p = "DELETE FROM Ordini WHERE Prodotto='$id' AND User='$user'";
           $conn->query($del_p);
         }
       }
    ?>
  </body>
</html>
