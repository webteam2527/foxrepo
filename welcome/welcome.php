<?php
include('login.php');
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WelcomePage</title>
    <link rel="stylesheet" href="welcome_css.css">
    <script src="jquery-3.2.1.min.js"></script>
    <script src="welcome.js"></script>
    <script src="../personalPage/openPage.js"></script>
  </head>
  <body>
  <header>
    <?php include("../shared/header_client.php"); ?>
  </header>
    <section>
      <h1 hidden>Welcomw</h1>
      <figure>
        <img src="welcome_fox_icon.png" alt="Image not available">
        <figcaption></figcaption>
      </figure>
      <span>Welcome!</span>
    </section>

    <section>
      <h1 hidden>User</h1>
      <form class="" action="login.php" method="post">
        <fieldset>
          <legend>Log in</legend>
          <p><label for="user"><input id="user" type="text" name="user" value="" placeholder="Username"></label></p>
          <p><label for="password"><input id="password" type="password" name="password" value="" placeholder="Password"></label></p>
          <input type="submit" name="login" value="Log in!">
        </fieldset>
      </form>
    </section>

    <section>
      <h1 hidden>SignUp</h1>
      <span>Don't have an account? Sign Up!</span>
    </section>

    <footer>
      <?php
      $mes="";
        if(isset($_GET['m'])){
          $mes = $_GET['m'];
        }
       ?>
      <span><?php echo $mes ?></span>
    </footer>
  </body>
</html>
