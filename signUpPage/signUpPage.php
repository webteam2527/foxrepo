<?php
  include('registrazione.php');
?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <title>signUpPage</title>
    <script src="jquery-3.2.1.min.js"></script>
    <!--<script src="backgroundInput.js"></script>-->
    <link rel="stylesheet" href="signUpPage.css">
  </head>
  <body>
    <form class="" action="registrazione.php" method="post">
      <fieldset>
        <legend>SignUp</legend>

        <fieldset class="form-group">
          <legend><span><img src="fox_icon.png" alt="icon" width="20" height="15"></span>Personal Information</legend>
        <input id="name" class="form-control" type="text" name="name" value="" placeholder="Name">
        <input id="surname" class="form-control" type="text" name="surname" value="" placeholder="Surname">
        <input id="age" class="form-control" type="number" name="age" value="" placeholder="Age" min="14" max="80">

        <label for="male"><input id="male" type="radio" name="radio" value="">Male</label>
        <label for="female"><input id="female" type="radio" name="radio" value="">Female</label>
      </fieldset>

      <fieldset class="form-group">
        <legend><span><img src="fox_icon.png" alt="icon" width="20" height="15"></span>Contacts</legend>
        <input id="address" class="form-control" type="text" name="address" value="" placeholder="Address">
        <input id="telephone" class="form-control" type="text" name="telephone" value="" placeholder="Telephone">
        <input id="mail" class="form-control" type="email" name="mail" value="" placeholder="E-mail">
      </fieldset>

      <fieldset class="form-group">
        <legend><span><img src="fox_icon.png" alt="icon" width="20" height="15"></span>Username &amp; Password</legend>
        <input id="user" class="form-control" type="text" name="user" value="" placeholder="Username">
        <input id="pass" class="form-control" type="password" name="pass1" value="" placeholder="Password">
        <input id="pass1" class="form-control" type="password" name="pass2" value="" placeholder=" Confirm Password">
      </fieldset>
    </fieldset>

      <div class="container">
      <input id="submit"  class="btn btn-info" type="submit" name="b1" value="SignUp!">
      <input id="clear" class="btn btn-info" type="reset" name="b2" value="Clear">
      </div>
    </form>
    <footer>
      <?php
      $mes = "";
      if(isset($_GET['m'])){
          $mes = $_GET['m'];
      }?>
      <span> <?php echo $mes ?> </span>
    </footer>
  </body>
</html>
