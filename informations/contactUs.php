<?php
include('../shared/conn.php');
 ?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>contactUs</title>
     <link rel="stylesheet" href="contactUs.css">
     <script src="jquery-3.2.1.min.js"></script>
     <script src="informations.js"></script>
   </head>
   <body>
     <header>
       <?php include('../shared/header_client.php') ?>
     </header>
     <section>
       <h1>Come puoi contattarci?</h1>
       <div>
         <ul>
           <li><a href="mailto:giulio.bacchilega@studio.unibo.it">giulio.bacchilega@studio.unibo.it</a></li>
           <li><a href="mailto:giulia.maglieri@studio.unibo.it">giulia.maglieri@studio.unibo.it</a></li>
         </ul>
       </div>
     </section>
   </body>
 </html>
