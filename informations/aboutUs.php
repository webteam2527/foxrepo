<?php
include('../shared/conn.php');
 ?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>aboutUs</title>
     <link rel="stylesheet" href="aboutUs.css">
     <script src="jquery-3.2.1.min.js"></script>
     <script src="informations.js"></script>
   </head>
   <body>
     <header>
       <?php include('../shared/header_client.php') ?>
     </header>
     <section>
       <h1>Chi Siamo?</h1>
       <article>
         <h2>Benvenuto in FoxEat</h2>
         <p>FoxEat nasce dalla volontà di creare emozioni ai clienti, attraverso la qualità dei prodotti, l’innovazione e una visione in continua evoluzione.
         Lo spazio è stato ideato per accogliere e unire un pubblico eterogeneo.
         Fedeli alla nostra filosofia, offriamo  un menù sano, fresco, “divertente” e innovativo, che guardi soprattutto al gusto dei piatti proposti, alla qualità e sostenibilità delle materie prime utilizzate.
         Quando si tratta di creare i nostri piatti, il nostro lavoro inizia nelle aziende agricole e nella natura. Vogliamo creare piatti deliziosi e per fare ciò utilizziamo le eccellenze del Italiane. Questo non significa necessariamente usare prodotti di lusso:
         la  differenza la devono fare le nostre mani, le nostre abilità e  la capacità di trasformare i prodotti  in emozioni.
         Cerchiamo ogni giorno di spingere il nostro menù ai confini del gusto, rimanendo fedeli alla nostra etica, usiamo solo prodotti sostenibili , di provenienza certa e garantita,  con una forte preferenza  per i  produttori locali.</p>
       </article>
     </section>
     <footer></footer>
   </body>
 </html>
