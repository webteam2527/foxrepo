<?php
  include('../shared/conn.php');

  $sql_n = "SELECT * FROM news";
  $res_n = $conn->query($sql_n);
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>HomePage</title>
    <link rel="stylesheet" href="home.css">
    <script src="jquery-3.2.1.min.js"></script>
    <script src="newsRotation.js"></script>
  </head>
  <body>
    <header>
      <div>
        <div>
          <p><span>Fox</span><span>Eat</span></p>
          <p>fast and smart, like a fox!</p>
        </div>
        <?php include("../shared/header_client.php") ?>
      </div>
      <div>
      </div>
    </header>
    <section id="presentation">
      <div>
        <h1>Chi siamo</h1>
      </div>
      <div>
        <h2>Ristorante d'asporto con cibi per tutti i palati!</h2>
        <p>Fox Eat nasce con l'intenzione di soddisfare tutti le categorie
           di clienti, con prodotti semplici ma gustosi, creati con passione
           utilizzando ingredienti di prima qualità, e consegnati a domicilio
           in tempi da record!
        </p>
      </div>
    </section>
    <?php include("news.php") ?>
    <script>
      var_n = <?php echo json_encode($v2); ?>;
    </script>
    <section id="ingredients">
      <div>
        <h1>I nostri ingredienti...</h1>
        <p></p>
      </div>
      <div>
        <div>
          <h2>I dolci...</h2>
          <p>I nostri dolci sono realizzati utilizzando latte, uova e farina
          100% italiani, di ottima qualità!</p>
        </div>
        <div>
          <h2>Il nostro pane...</h2>
          <p>Abbiamo pane integrale, pane bianco, montanaro e pane da hot dog,
          tutti rigorosamente prodotti nei nostri forni utilizzando le migliori materie prime.
          Da provare!</p>
        </div>
        <div>
          <h2> I cibi etnici...</h2>
          <p> Con ingredienti provenienti dalle regioni tipiche, come i fagioli neri,
          l'avocado, o i wurstel belgi, riusciamo a proporvi piatti delle varie cucine etniche, come
          strabilianti hot dogs, succulenti burritos o deliziosi sandwich</p>
        </div>
        <div>
          <h2>Pizza</h2>
          <p>Cosa c'è di meglio di una buona pizza? Seguendo meticolosamente l'antica tradizione
          napoletana, possiamo offrirvi una pizza dall'impasto leggero, soffice e soprattutto gustoso.
          Cosa state aspettando?</p>
        </div>
      </div>
    </section>
  </body>
</html>
