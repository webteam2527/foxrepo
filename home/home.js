var NCat = 8;
var burghers = new Array();
var hotdogs = new Array();
var sandwitches = new Array();
var pizza = new Array();
var salads = new Array();
var burritos = new Array();
var frieds = new Array();
var sweets = new Array();
var drinks = new Array();

var count = 0;
var countCat = 0;
var c_list = 0;

var MAX = 3;

$(document).ready(function(){
  showCat();
  show();
});

function nextCat(){
  countCat++;
  if(countCat > NCat){
    countCat = 0;
  }
  showCat();
  show();
}

function prevCat(){
  countCat--;
  if(countCat < 0){
    countCat = NCat;
  }
  showCat();
  show();
}

function next(){
  categories[countCat].n();
  show();
}

function showCat(){
  $("#ftype > div div:first-child p").html(categories[countCat].getName());
}

function prev(){
  categories[countCat].p();
  show();
}

function show(){
  $("#ftype div:last-child img:first-child").attr("src", categories[countCat].getPath()+categories[countCat].getPreviousFood().getSrc());
  $("#ftype div:last-child img:nth-child(2)").attr("src", categories[countCat].getPath()+categories[countCat].getActualFood().getSrc());
  $("#ftype div:last-child img:nth-child(3)").attr("src", categories[countCat].getPath()+categories[countCat].getNextFood().getSrc());

  $("#fdesc h1").html(categories[countCat].getActualFood().getName());
  $("#fdesc div > div:first-child img").attr("src",categories[countCat].getPath()+categories[countCat].getActualFood().getSrc());
  $("#fdesc div > div:last-child p:first-child").html(categories[countCat].getActualFood().getDesc());
  $("#fdesc div > div:last-child p:nth-child(2)").html(categories[countCat].getActualFood().getIng());
  $("#fdesc div > div:last-child span p").html(categories[countCat].getActualFood().getPrice());
}

function Category(id, src, name, nFood, vet){
  this.id = id;
  this.src = src;
  this.name = name;
  this.nFood = nFood;
  this.array = vet;

  this.my_count = 0;
  this.c_list = 0;

  this.getNext = function(){
    if(this.c_list+1 > this.nFood-1){
      return 0 ;
    }
    return this.c_list+1;
  }

  this.getPrev = function(){
    if(this.c_list-1 < 0){
      return this.nFood - 1;
    }
    return this.c_list -1;
  }

  this.p = function(){
    this.c_list = this.getPrev();
  }

  this.n = function(){
    this.c_list = this.getNext();
  }

  this.getPath = function(){
    return this.src;
  }

  this.getName = function(){
    return this.name;
  }

  this.addFood = function(f){
    this.array[this.my_count] = f;
    this.my_count++;
  }

  this.getPreviousFood = function(){
    return this.array[this.getPrev()];
  }

  this.getActualFood = function(){
    return this.array[this.c_list];
  }

  this.getNextFood = function(){
    return this.array[this.getNext()];
  }
}

var burg = new Category(0, "../FoodTypes/Hamburger/", "Hamburgers", 4, burghers);
var hot = new Category(1, "../FoodTypes/HotDog/", "Hot dogs", 4, hotdogs);
var sand = new Category(2, "../FoodTypes/Sandwitch/", "Sandwiches", 4, sandwitches);
var pizza = new Category(3, "../FoodTypes/Pizza/", "Pizza", 10, pizza);
var salad = new Category(3, "../FoodTypes/Insalate/", "Salads", 10, salads);
var burrito = new Category(3, "../FoodTypes/Burrito/", "Burritos", 7, burritos);
var fried = new Category(3, "../FoodTypes/Fritti/", "Frieds", 7, frieds);
var sweet = new Category(3, "../FoodTypes/Dolci/", "Sweets", 26, sweets);
var drink = new Category(3, "../FoodTypes/Bevande/", "Drinks", 8, drinks);

var categories = new Array(burg, hot, sand, pizza, salad, burrito, fried, sweet, drink);

function append(f){
  categories[f.getCat()].addFood(f);
}

function SingleFood(cat, nome, src, descrizione, ingredienti, prezzo){
  this.cat = cat;
  this.nome = nome;
  this.descrizione = descrizione;
  this.src = src;
  this.ingredienti = ingredienti;
  this.prezzo = prezzo;

  this.getName = function(){
    return this.nome;
  }
  this.getSrc = function(){
    return this.src;
  }
  this.getDesc = function(){
    return this.descrizione;
  }
  this.getIng = function(){
    return this.ingredienti;
  }
  this.getPrice = function(){
    return this.prezzo;
  }
  this.getCat = function(){
    return this.cat;
  }
}

var hamburger = new SingleFood(0,"Hamburger", "hamburger.jpg", "Hamburger semplice", "100 g di carne chianina, pomodoro, insalata, cipolla", "5.00");
append(hamburger);
var cheese_b = new SingleFood(0, "Cheese Burger", "cheeseburger.jpg", "Hamburger con formaggio cheddar", "100 g di carne chianina, formaggio cheddar, pomodoro, insalata, cipolla", "6.00");
append(cheese_b);
var chicken_b = new SingleFood(0, "Chicken Burger", "chicken_burger.jpg", "Hamburger di pollo fritto", "100 g di pollo fritto, pomodoro, insalata, cipolla", "4.00");
append(chicken_b);
var diablo_b = new SingleFood(0, "Diablo Burger", "diablo_burger.jpg", "Hamburger in salsa piccante e pane al peperoncino", "Pane al peperoncino, 100 g di carne chianina, formaggio cheddar, pomodoro, insalata, cipolla, salsa diablo", "5.00");
append(diablo_b);
var bacon_b = new SingleFood(0, "Bacon Burger", "bacon_cheeseburgher.jpg", "Hamburger con bacon ", " 100 g di carne chianina, bacon croccante, formaggio cheddar, pomodoro, insalata, cipolla", "4.50");
append(bacon_b);
var double_b = new SingleFood(0, "Double Burger", "double_cheeseburger.jpg", "Hamburger Double", " 200 g di carne chianina,  formaggio cheddar, pomodoro, insalata, cipolla", "6.00");
append(double_b);
var fish_b = new SingleFood(0, "Fish Burger", "fish_burger.jpg", "Hamburger con pesce ", " cotoletta di pesce, formaggio cheddar, pomodoro, insalata, cipolla, maionese", "5.50");
append(fish_b);
var italian_b = new SingleFood(0, "Italian Burger", "italian_burger.jpg", "Hamburger all'italiana ", " 100 g di carne chianina, fonduta di taleggio, pomodoro, insalata, rucola, cipolla", "4.00");
append(italian_b);
var shirmp_b = new SingleFood(0, "Shirmp Burger", "shirmp_burger.jpg", "Hamburger con gamberetti ", " gamberetti alla griglia,verdure miste, salsa tzatziki", "5.50");
append(shirmp_b);
var titan_b = new SingleFood(0, "Titan Burger", "titan_burger.jpg", "Hamburger Maxi ", " 300 g di carne chianina, bacon croccante, formaggio cheddar, formaggio fuso, patate croccanti pomodoro, insalata, cipolla carammelata, salsa barbecue", "6.50");
append(titan_b);
var vegan_b = new SingleFood(0, "Vegan Burger", "vegan_burger.jpg", "Hamburger vegan ", " 100 g di hamburger di soia, pomodoro, insalata, cipolla", "3.50");
append(vegan_b);

var avocado_s = new SingleFood(2, "Avocado Sandwitch", "avocado_sandwitch.jpg", "Sandwitch con avocado", "Pane in cassetta, pollo e avocado", "3.00");
append(avocado_s);
var carbonara_s = new SingleFood(2, "Carbonara Sandwitch", "carbonara_sandwitch.jpeg", "Sandwitch con bacon e uova strapazzate", "Pane in cassetta, bacon, uova e formaggio", "5.00");
append(carbonara_s)
var egg_s = new SingleFood(2, "Egg Sandwitch", "egg_sandwitch.jpg", "Sandwitch alle uova", "Pane in cassetta e uova fritte", "3.50");
append(egg_s);
var italian_s = new SingleFood(2, "Italian Sandwitch", "italian_sandwitch.jpg", "Sandwitch con ingredienti della dieta mediterranea", "Pane in cassetta, pesto alla genovese, pomodorini, basilico e prosciutto croccante", "5.50");
append(italian_s);
var ham_s = new SingleFood(2, "Ham Sandwitch", "ham_sandwitch.jpg", "Sandwitch con prosciutto crudo", "Pane in cassetta, prosciutto crudo, formaggio stagionato e insalata", "4.50");
append(ham_s);
var mushrooms_s = new SingleFood(2, "Mushroom Sandwitch", "mushrooms_sandwitch.jpg", "Sandwitch con funghi", "Pane in cassetta, funghi in padella ed erbe", "5.00");
append(mushrooms_s);
var shirmp_s = new SingleFood(2, "Shirmp Sandwitch", "shrimp_sandwitch.jpg", "Sandwitch con gamberetti", "Pane in cassetta, gamberetti, salsa rosa e insalata", "5.50");
append(shirmp_s);
var vegan_s = new SingleFood(2, "Vegan Sandwitch", "vegan_sandwitch.jpg", "Sandwitch vegano", "Pane in cassetta, tofu, germogli di soia e verdure varie", "4.50");
append(vegan_s);

var avocado_h = new SingleFood(1, "Avocado HotDog", "AvocadoHotDog.jpg", "HotDog Dog con avocado", "Pane, wurstel e avocado", "4.00");
append(avocado_h);
var kebab_h = new SingleFood(1, "Kebab HotDog", "KebabHotDog.jpg", "Hot Dog al Kebab", "Pane, wurstel e carne di kebab", "5.00");
append(kebab_h);
var classic_h = new SingleFood(1, "Classic HotDog", "ClassicHotDog.jpg", "Hot Dog americano", "Pane, wurstel e mostarda", "3.50");
append(classic_h);
var vegan_h = new SingleFood(1, "Vegan HotDog", "VeganHotDog.jpg", "Hot Dog vegano", "Pane, wurstel di tofu e verdure", "3.00");
append(vegan_h);
var vegetable_h = new SingleFood(1, "Vegetable HotDog", "VegetableHotDog.jpg", "Hot Dog con verdure", "Pane, wurstel e verdure miste", "3.50");
append(vegetable_h);

var americana_p = new SingleFood(3, "Pizza Americana ", "Americana.jpg", "Pizza con wurstel e patatite", "Base margherita con wurstel e patatine", "4.50");
append(americana_p);
var calzone_p = new SingleFood(3, "Calzone", "Calzone.jpg", "Pizza chiusa", "Base margherita con funghi e salsiccia ", "5.50");
append(calzone_p);
var capricciosa_p = new SingleFood(3, "Pizza Capricciosa", "Capricciosa.jpg", "Pizza con carciofi prosciutto olive ", "Base margherita con carciofi, olive nere e prosciutto cotto", "6.00");
append(capricciosa_p);
var diavola_p = new SingleFood(3, "Pizza Diavola", "Diavola.jpg", "Pizza con salame piccante", "Base margherita con salame piccante, olive nere e salsa al peperoncino", "5.50");
append(diavola_p);
var funghisalsiccia_p = new SingleFood(3, "Pizza Funghi&Salsiccia", "FunghiSalsiccia.jpg", "Pizza con funghi e salsiccia", "Base margherita con funghi e salsiccia", "4.50");
append(funghisalsiccia_p);
var margherita_p = new SingleFood(3, "Pizza Margherita", "Margherita.jpg", "Pizza con mozzarella e pomodoro", "Pizza con mozzarella e pomodoro", "3.50");
append(margherita_p);
var mediterranea_p = new SingleFood(3, "Pizza Mediterranea", "Mediterranea.jpg", "Pizza capperi e acciughe", "Base pomodoro con capperi sotto sale e acciughe", "4.00");
append(mediterranea_p);
var quattroformaggi_p = new SingleFood(3, "Pizza 4 formaggi", "QuattroFormaggi.jpg", "Pizza con selezione di 4 formaggi", "Base bianca con gorgonzola, taleggio, grana e groviera", "5.50");
append(quattroformaggi_p);
var quattrostagioni_p = new SingleFood(3, "Pizza 4 stagioni", "QuattroStagioni.jpg", "Pizza con prosciutto, funghi, olive e carciofi", "Base margherita divisa in 4 parti con olive, funghi, prosciutto cotto e carciofi", "6.50");
append(quattrostagioni_p);
var vegan_p = new SingleFood(3, "Pizza vegana", "Vegan.jpg", "Pizza con verdure", "Impasto di crusca, base pomodoro e verdure miste ", "4.50");
append(vegan_p);

var avocado_s = new SingleFood(4, "Avocado Salad", "AvocadoSalad.jpg", "Insalata con avocado", "Insalata mista, pomodoro, peperoni, cipolla e avocado", "5.00");
append(avocado_s);
var caesar_s = new SingleFood(4, "Caesar Salad", "CaesarSalad.jpg", "Insalata con pollo ai ferri", "Insalata mista, pollo ai ferri e salsa caesar", "6.00");
append(caesar_s);
var caesarshirmp_s = new SingleFood(4, "Caesar Shirmp Salad", "CaesarSaladwithShrimp.jpg", "Insalata con gamberetti grigliati", "Insalata mista, pomodoro, gamberetti e salsa rosa", "6.50");
append(caesarshirmp_s);
var classic_s = new SingleFood(4, "Classic Salad", "ClassicSalad.jpg", "Insalata", "Insalata mista, pomodoro, peperoni, cipolla", "3.50");
append(classic_s);
var greek_s = new SingleFood(4, "Greek Salad", "GreekSalad.jpg", "Insalata con feta", "Insalata mista, pomodoro, peperoni, cipolla, olive nere e feta", "5.50");
append(greek_s);
var pinapple_s = new SingleFood(4, "Pinapple Salad", "GrilledPinappleSalad.jpg", "Insalata con ananas", "Insalata mista e ananas grigliata", "4.00");
append(pinapple_s);
var caprese_s = new SingleFood(4, "Caprese Salad", "InsalataCaprese.jpg", "Insalata con mozzarella", "Pomodoro a fette, mozzarella di bufala, origano e basilico", "5.00");
append(caprese_s);
var farro_s = new SingleFood(4, "Farro Salad", "InsalataDiFarro.jpg", "Insalata di farro", "Farro e verdure miste tagliate sottili", "4.00");
append(farro_s);
var polpo_s = new SingleFood(4, "Polpo Salad", "InsalataPolpo.jpg", "Insalata con polpo", "Polpo bollito e poi marinato  e patate", "5.50");
append(polpo_s);
var potato_s = new SingleFood(4, "Potato Salad", "PotatoSalad.jpg", "Insalata di patate", "Insalata mista e patate", "4.50");
append(potato_s);

var kebab_b = new SingleFood(5, "Kebab Burrito", "BurritoKebab.jpg", "Burrito al kebab", "Burrito con verdure miste e carne di kebab", "5.50");
append(kebab_b);
var friedchicken_b = new SingleFood(5, "Fried Chicken Burrito", "FriedChickenBurrito.jpg", "Burrito con pollo fritto", "Burrito con verdure miste e pollo fritto e riso", "6.00");
append(friedchicken_b);
var hotspicy_b = new SingleFood(5, "Hot Spicy Burrito", "HotSpicyBurrito.jpg", "Burrito alle spezie", "Burrito con fagioli, salsa piccante, peperoncino, pomodoro e peperoni", "5.00");
append(hotspicy_b);
var meatball_b = new SingleFood(5, "Meat Ball Burrito", "MeatBallBurrito.jpg", "Burrito con polpette", "Burrito con verdure miste,polpette di carne e sugo", "4.50");
append(meatball_b);
var mediterrean_b = new SingleFood(5, "Meditterean Burrito", "MediterreanBurrito.jpg", "Burrito mediterraneo", "Burrito con ingredienti della dieta mediterranea", "5.00");
append(mediterrean_b);
var sushi_b = new SingleFood(5, "Sushi Burrito", "SushiBurrito.jpg", "Burrito sushi", "Burrito con impasto al nero di seppia, riso, pesce e verdure", "6.50");
append(sushi_b);
var vegan_b = new SingleFood(5, "Vegan Burrito", "VeganBurrito.jpg", "Burrito vegano", "Burrito con verdure miste e tofu", "3.50");
append(vegan_b);

var chicken_n = new SingleFood(6, "Chicken Nuggets", "ChickenNuggets.jpg", "Crocchette di pollo (6 pz)", "Crocchette di pollo impanate e fritte", "4.00");
append(chicken_n);
var chicken_w = new SingleFood(6, "Chicken Wings", "ChickenWings.jpg", "Alette di pollo (6 pz)", "Alette di pollo fritte e coperte di salsa barbecue", "5.00");
append(chicken_w);
var crock_p = new SingleFood(6, "Potato Crock", "CrocchettePatate.jpg", "Crocchette di patate (4 pz)", "Crocchette di patate fritte", "3.00");
append(crock_p);
var mozz_p = new SingleFood(6, "Mozzarella Balls", "MozzarellePanate.jpg", "Mozzarelle panate (4 pz)", "Crocchette di mozzarella impanate e fritte", "4.00");
append(mozz_p);
var olive_a = new SingleFood(6, "Ascolana Olives", "OliveAscolane.jpg", "Olive all'ascolana (6 pz)", "Olive ripiene di carne, impanate e fritte", "4.00");
append(olive_a);
var onion_r = new SingleFood(6, "Onion Rings", "OnionRings.jpg", "Anelli di Cipolla(5 pz)", "Anelli di cipolla bianca impanati e fritti", "3.50");
append(onion_r);
var chips_f = new SingleFood(6, "Fried Potato Sticks", "PatatineFritte.jpg", "Patatine fritte (confezione media)", "Patate tagliate sottili e fritte", "2.50");
append(chips_f);

var apple_p = new SingleFood(7, "Apple Pie", "ApplePie.jpg", "Torta di mele", "Ciambella bianca con mele", "1.00 (1 fetta)");
append(apple_p);
var banana_c = new SingleFood(7, "Banana Cookies", "BananaCookies.jpg", "Biscotti alla banana", "Biscotti con impasto alla banana", "3.00 (4 pezzi)");
append(banana_c);
var bananaraspchoc_m = new SingleFood(7, "BananaRaspberryChoc Muffin", "BananaRaspChocMuffin.jpg", "Muffin con banana, lamponi e cioccolato", "Muffin con impasto alla banana e pezzi di lampone e cioccolato", "3.00 (2 pezzi)");
append(bananaraspchoc_m);
var blueberry_m = new SingleFood(7, "Blueberry Muffin", "BlueberryMuffin.jpg", "Muffin ai mirtilli", "Muffin con pezzi di mirtilli", "3.00 (2 pezzi)");
append(blueberry_m);
var bluerasp_m = new SingleFood(7, "BlueRaspberry MilkShake", "BlueRaspberryMilkshake.jpg", "Milkshake alle more", "Frullato con latte e succo di more e more in pezzi", "1.50");
append(bluerasp_m);
var choc_b = new SingleFood(7, "Choc Brownies", "Brownies.jpg", "Brownies al cioccolato", "Brownies con burro e cioccolato fondente", "2.00 (2 pezzi)");
append(choc_b);
var butterchock_c = new SingleFood(7, "Butter Choc Cookies", "ButterChocCoockies.jpg", "Biscotti con burro e cioccolato", "Biscotti con burro e gocce di cioccolato", "3.00 (4 pezzi)");
append(butterchock_c);
var carrot_c = new SingleFood(7, "Carrot Cake", "CarrotCake.jpg", "Torta di carote", "Ciambella con carote nell'impasto", "1.00 (1 fetta)");
append(carrot_c);
var rasp_c = new SingleFood(7, "Raspberry Cheesecake", "CheesecakeLamp.jpg", "Cheesecake al lampone", "Cheesecake con guarnizione al lampone", "2.00 (1 fetta)");
append(rasp_c);
var caramel_c = new SingleFood(7, "Caramel Cheesecake", "CheesecakeCaramel.jpg", "Torta al caramello", "Cheesecake con guarnizione al caramello", "2.00 (1 fetta)");
append(caramel_c);
var choc_c = new SingleFood(7, "Choc Cookies", "ChocCoockies.jpg", "Biscotti al cioccolato", "Biscotti al cioccolato con gocce di cioccolato", "3.00 (4 pezzi)");
append(choc_c);
var choc_d = new SingleFood(7, "Choc Donut", "ChocoDonut.jpg", "Ciambellina al cioccolato", "Ciambellina con guarnizione al cioccolato", "0.50 (1 pezzo)");
append(choc_d);
var choc_m = new SingleFood(7, "Choc Milkshake", "ChocolateMilkshake.jpg", "Milkshake al cioccolato ", "Milkshake al cioccolato con panna fresca", "1.50");
append(choc_m);
var choc_mu = new SingleFood(7, "Chocolate Muffin", "ChocolateMuffin.jpg", "Muffin al cioccolato", "Muffin al cioccolato", "3.00 (2 pezzi)");
append(choc_mu);
var foresta_n = new SingleFood(7, "Foresta Nera", "ForestaNera.jpg", "Torta al cioccolato", "Torta con guarnizione e ripieno al cioccolato", "1.80 (1 fetta)");
append(foresta_n);
var lemon_b = new SingleFood(7, "Lemon Brownies", "LemonBrownies.jpg", "Brownies al limone", "Brownies al limone e cioccolato bianco", "2.00 (2 pezzi)");
append(lemon_b);
var mint_b = new SingleFood(7, "Mint Brownies", "MintChocBrownies.jpg", "Brownies alla menta", "Brownies alla menta e cioccolato fondente", "2.00 (2 pezzi)");
append(mint_b);
var oreo_b = new SingleFood(7, "Oreo Brownies", "OreoBrownies.jpg", "Brownies agli Oreo", "Brownies al cioccolato con ripieno al latte e scaglie di cioccolato", "2.00 (2 pezzi)");
append(oreo_b);
var penaut_c = new SingleFood(7, "Peanut Butter Cookies", "PenautButterCookies.jpg", "Biscotti al burro d'arachidi", "biscotti con burro d'arachidi e anacardi in pezzi", "3.00 (4 pezzi)");
append(penaut_c);
var fruit_c = new SingleFood(7, "Red Fruit Cookies", "RedFruitCookies.jpg", "Biscotti ai frutti rossi", "Biscotti al burro con pezzi di frutti rossi", "3.00 (4 pezzi)");
append(fruit_c);
var sacher = new SingleFood(7, "Sacher", "Sacher.jpg", "Torta Sacher", "Torta al cioccolato con ripieno di confettura all'albicocca", "2.00 (1 fetta)");
append(sacher);
var straw_d = new SingleFood(7, "Strawberry Donuts", "StrawberryDonut.jpg", "Ciambellina alle fragole", "Ciambellina con guarnizione e ripieno di fragole", "0.50 (1 pezzo)");
append(straw_d);
var straw_m = new SingleFood(7, "Strawberry Milkshake", "StrawberryMilkshake.jpg", "Milkshake alle fragole", "MilkShake alle fragole con panna fresca", "1.50");
append(straw_m);
var oreo_c = new SingleFood(7, "Oreo Cake", "TortaOreo.jpg", "Torta Oreo", "Torta al cioccolato e latte con biscotti oreo", "2.50 (1 fetta)");
append(oreo_c);
var vanilla_m = new SingleFood(7, "Vanilla Muffin", "VanillaMuffin.jpg", "Muffin alla vaniglia", "Muffin con ripieno alla vaniglia", "3.00 (2 pezzi)");
append(vanilla_m);
var white_d = new SingleFood(7, "White Donut", "WhiteDonut.jpg", "Ciambellina alla panna", "Ciambella bianca con guarnizione e ripieno alla panna", "0.50 (1 pezzo)");
append(white_d);

var natural_w = new SingleFood(8, "Natural Water", "acqua_n.jpg", "Acqua Naturale 0.5 cl", "", "0.40");
append(natural_w);
var sparkling_w = new SingleFood(8, "Sparkling Water", "acqua-f.jpg", "Acqua Frizzante 0.5 cl", "", "0.40");
append(sparkling_w);
var birra = new SingleFood(8, "Birra", "birramoretti.jpg", "Birra Moretti 0.33 cl", "", "1.30");
append(birra);
var cocaCola = new SingleFood(8, "Coca-Cola", "coca_cola.jpeg", "Coca-Cola 0.5 cl", "", "1.20");
append(cocaCola);
var fanta = new SingleFood(8, "Fanta", "fanta.jpg", "Fanta 0.5 cl", "", "1.20");
append(fanta);
var sprite = new SingleFood(8, "Sprite", "sprite.jpg", "Sprite 0.5 cl", "", "1.40");
append(sprite);
var lemon_te = new SingleFood(8, "Lemon The", "te-limone.jpg", "Te al limone 0.5 cl", "", "1.40");
append(lemon_te);
var peach_te = new SingleFood(8, "Peach The", "te-pesca.jpg", "Te alla pesca 0.5 cl", "", "1.40");
append(peach_te);
