var count = 0;

$(function(){
	sticky_f($("#presentation > div:first-child"));
  sticky_f($("#news > div:first-child"));
  sticky_f($("#ingredients > div:first-child"));
  sticky_s($("#ftype > div:first-child"));
});

function sticky_f(sticky) {
   var	pos = sticky.offset().top;
   var step;
	 win = $(window);
   if(count === 0){
     step = pos;
   } else{
     step = pos - 150;
   }
	 win.on("scroll", function() {
    	win.scrollTop() >= step ? sticky.addClass("fixed") : sticky.removeClass("fixed");
	 });
   count++;
}

function sticky_s(sticky) {
   var	pos = sticky.offset().top;
	 win.on("scroll", function() {
    	win.scrollTop() >= pos -400 ? sticky.addClass("fixed") : sticky.removeClass("fixed");
	 });
}
