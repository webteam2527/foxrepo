$(document).ready(function(){
    setInterval(rotate, 5000);
    $("input[name=before]").click(prev);
    $("input[name=after]").click(next);
    $("#logout").hide();
    $("#cart").hide();
    $("#admin").hide();
    $("#home").hide();
    $("#head > div:first-child figure").hide();

var count = 0;
var path_im = 4;
var path_desc = 2;

function rotate() {
  hideCurr();
  showNext();
  /* count = getNext();
    $("#im_news").attr("src", var_n[count][path_im]);
    $("#desc_news").html(var_n[count][path_desc]);*/
  }

  function hideCurr(){
    $("#im_news").attr('src', var_n[count][path_im]).hide();
    $("#desc_news").html(var_n[count][path_desc]).hide();
  }

  function showPrev(){
      count = getPrev();
      $("#im_news").attr('src',var_n[count][path_im]).fadeIn("slow");
      $("#desc_news").html(var_n[count][path_desc]).fadeIn("slow");
  }

  /* funzione di controllo quando sono al primo indice*/
  function getPrev(){
    if(count == 0){
      return count = var_n.length-1;
    }else{
      return count - 1;
    }
  }

  /* funzione chiamata dal bottone*/
  function prev(){
      hideCurr();
      showPrev();
  }

  function showNext(){
      count = getNext();
      $("#im_news").attr('src', var_n[count][path_im]).fadeIn("slow");
      $(" #desc_news").html(var_n[count][path_desc]).fadeIn("slow");
  }

  /* funzione di controllo quando sono alla fine dell'array*/
  function getNext(){
    if(count == var_n.length-1){
      count = 0;
      return count;
    }else{
      return count + 1;
    }
  }
  /* funzione richiamata dal bottone*/
  function next(){
      hideCurr();
      showNext();
  }



});
